<?php include 'includes/header.php'?>
<div class="colorlib-loader"></div>
<div id="page">
    <div class="colorlib-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
                    <div class="intro">
                        <h1>Veja como é fácil se livrar de <br/><span style="color: #a2dc4d">6 a 11kg em Apenas 28 dias</span></h1>
                        <p style="margin-bottom: -20px !important"><b>Sem passar Fome e sem Dietas Malucas!</b> (ASSISTA O VÍDEO)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <aside id="colorlib-hero">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider">
                        
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="" class="embed-responsive-item" allowfullscreen="allowfullscreen" data-src="https://www.youtube.com/embed/a2DnX8sMjcc?controls=1git" allowfullscreen></iframe>
                        </div>
                           
                      </div>
                      <p class="text-center" style="margin-top: 15px; color: #fff;">
                        Acesso vitalício por <b>apenas R$ 35,00</b>!<br/>
                        Pague à vista ou parcele no cartão<br/>
                        
                    </p>
                    <p class="text-center">
                        <a href="https://sun.eduzz.com/300553" target="_blank" class="btn btn-primary btn-outline btn-md">SIM! EU QUERO</a> 
                    </p>	
                  </div>
              </div>
          </div>
    </aside>

    <div id="colorlib-services">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
                    <h2>Para dar certo é só seguir esses passos:</h2>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="icon-calendar"></i>
                        </span>
                        <div class="desc">
                            <h3>PASSO 1</h3>
                            <p>Conhecer e seguir nosso super cronograma de 28 dias corretamente</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="icon-clock"></i>
                        </span>
                        <div class="desc">
                            <h3>PASSO 2</h3>
                            <p>Programar seu tempo para conseguir fazer e comer direitinho cada receita </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <i class="icon-book-open"></i>
                        </span>
                        <div class="desc">
                            <h3>PASSO 3</h3>
                            <p>Ler nosso material e dar uma olhada nas dezenas de receitas antes de começar</p>
                            </div>
                    </div>
                </div>
                <div class="col-md-offset-3 col-md-6 col-sm-12 text-center">
                    <p style="line-height: 18px;"><small><b>Já ajudamos mais de 8.741 pessoas no Brasil e mais 9 países, homens e mulheres de todas as idades que tomaram a decisão de emagrecer com nosso método exclusivo e simples</b></small></p>
                </div>
            </div>
        </div>
    </div>


    <div id="colorlib-counter" class="colorlib-counters" style="background-image: url(images/cover_img_1.webp);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
                    <h2 style="margin-bottom: 0px;">O livro de receitas<br/> mais vendido do ano passado</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-md-offset-5">
                    <div class="col-md-6 col-sm-6 text-center animate-box">
                        <div class="counter-entry">
                            <div class="desc" style="color: #fff">
                                Já somos <span class="colorlib-counter js-counter" data-from="0" data-to="9112" data-speed="5000" data-refresh-interval="50"></span>
                                <span class="colorlib-counter-label">Clientes</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="colorlib-services">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center colorlib-heading animate-box">
                    <h3>As melhores receitas do Brasil</h3>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-12 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <img src="images/1.webp" loading="lazy" width="200" height="200">
                        </span>
                        <div class="desc">
                            <h3>Receitas DETOX</h3>
                            <p>Feitas para eliminar as toxinas de nosso organismo. É muito importante para o processo de perda de peso se livrar de gorduras ruins e toxinas.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <img src="images/2.webp" loading="lazy" width="200" height="200">
                        </span>
                        <div class="desc">
                            <h3>Receitas LOWCARB</h3>
                            <p>Ingerir menos carboidratos simples fazem com que você reduza triglicerídeos. Isso faz com que você emagreça de forma rápida e saúdavel. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <img src="images/3.webp" loading="lazy" width="200" height="200">
                        </span>
                        <div class="desc">
                            <h3>Receitas Sopas FIT</h3>
                            <p>Além de deliciosas e elaboradas para que você consiga facilmente substituir o jantar, eliminando assim até 2kg / semana.</p>
                            </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 text-center animate-box">
                    <div class="services">
                        <span class="icon">
                            <img src="images/4.webp" loading="lazy" width="200" height="200">
                        </span>
                        <div class="desc">
                            <h3>Chás & Bebidas FIT</h3>
                            <p>Temos as receitas das bebidas e chás mais poderosos e com efeitos já na primeira semana. Todos os ingredientes são fáceis de comprar.</p>
                            </div>
                    </div>
                </div>
                <div class="col-md-offset-2 col-md-8 col-sm-12 text-center">
                    <p style="line-height: 18px;"><small><b>Além de tudo isso...</b><br/> Você terá acesso a receitas para refeições práticas, rápidas, baratas e <br/>capazes de fazer você eliminar os kilinhos que tanto incomodam!<br/><br/><b>São mais de 120 receitas para secar, desinchar e emagrecer em 28 dias.</b></small></p>
                </div>
            </div>
        </div>
    </div>


    <div class="colorlib-intro" style="margin-top: 40px; height: 400px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
                    <div class="intro" style="position: relative; top: -80px;">
                        <img src="images/iphone-mockup.png" loading="lazy">
                        <h1 style="font-size: 24px;">Acesse suas receitas <br/>pelo Celular sempre que quiser</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="colorlib-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box" style="margin-bottom: 30px;">
                    <h2>As receitas farão você...</h2>
                    <p style="margin-bottom: 0px; text-align: left;">
                        Emagrecer até 11kg em 28 dias <br/>
                        Diminuir ansiedade de comer toda hora (principalmente doces) <br/>
                        Acelerar seu metabolismo <br/>
                        Eliminar inchaços de pernas, braços e barriga <br/>
                        Reduzir de 3 a 15cm de barriga <br/>
                        Conquistar o corpo que merece <br/>
                        Ter uma pele mais limpa e saudável <br/>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div id="colorlib-testimony" style="background-color: #232525;" class="testimony-img">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="row animate-box">
                        <div class="owl-carousel1">
                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/eliseu.png);"></figure>
                                        <blockquote>
                                            <span>Vicente Ferraz</span>
                                            <p>"Estou muito feliz com o resultado! No começo estava duvidando das receitas, mas em pouco tempo percebi a diferença. Super indico!"</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/cesar.png);"></figure>
                                        <blockquote>
                                            <span>Geraldo Rivera</span>
                                            <p>"O Guia do Corpo Perfeito foi um grande achado para mim. Nessas 26 receitas para secar, pude desfrutar da alegria de entrar naquele jeans que não servia mais há muito tempo."</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/layon.png);"></figure>
                                        <blockquote>
                                            <span>Leandro Martins</span>
                                            <p>"Simplesmente incrível!!! Superou minhas expectativas. É só seguir a receita que o resultado vem!!"</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/rafa.png);"></figure>
                                        <blockquote>
                                            <span>Rosélia S. Bertine</span>
                                            <p>"Essas receitas são milagrosas hahaha, nem acredito que conseguir ter esses resultados em tão pouco tempo."</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/isa.png);"></figure>
                                        <blockquote>
                                            <span>Gabrielle Siqueira</span>
                                            <p>"Valeu muito a pena o investimento. Com 5 dias de uso, já vi a diferença no espelho."</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/luiz.png);"></figure>
                                        <blockquote>
                                            <span>Carlos Gonzaga</span>
                                            <p>"O guia de 26 receitas para emagrecer tem me ajudado muito. Estou seguindo à risca e o resultado já começou a aparecer."</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimony-slide">
                                    <div class="testimony-wrap">
                                        <figure class="figure-img" style="background-image: url(images/roger.png);"></figure>
                                        <blockquote>
                                            <span>Lauro Barbosa</span>
                                            <p>"Depois de muito foco e muita determinação comecei a seguir a risca uma dieta, mas foi quando eu li esse ebook que meus resultados aumentaram de uma forma surpreendente..."</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="colorlib-pricing">
        <div class="container">				
            <div class="row">
                <div class="col-md-offset-4 col-md-4 text-center">
                    <div class="pricing">
                        <div class="price"><sup class="currency">R$</sup>35,00<small>COMPRA 100% SEGURA</small></div>
                        <p><a href="https://sun.eduzz.com/300553" target="_blank" class="btn btn-lg btn-primary"><b>COMPRAR AGORA</b></a></p>
                        <p>Receba <b>IMEDIATAMENTE</b> seu material após confirmação do pagamento</p>
                        
                        <img src="images/selo-garantia.png" loading="lazy" style="margin-top: -10px; margin-bottom: -10px; max-width: 120px;">
                        <p style="margin-top: 15px;line-height: 18px;"> <small>Compre com tranquilidade!<br/>Garantia de 7 dias<br/><b>Devolvemos seu dinheiro se não gostar!</b></small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include 'includes/footer.php'?>
